﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public List<Camera> Cameras;
	public KeyCode CameraKeyCode;

	private int CameraIndex;

	// Use this for initialization
	void Start () {
		this.CameraIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (CameraKeyCode)) {
			this.Cameras.ForEach (c => {
				c.gameObject.SetActive(false);
			});
			this.Cameras [CameraIndex].gameObject.SetActive (true);
			this.CameraIndex++;
			if (this.CameraIndex > (this.Cameras.Count - 1)) {
				this.CameraIndex = 0;
			}
		}
	}
}
